import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../assets/css/CompanyList.css';
import { useNavigate } from 'react-router-dom';
import Logo from '../assets/img/logo.png';
import { useLocation } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

const ProductList = (props) => {
  const location = useLocation();
  const [companies, setCompanies] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedCompany,setSelectedCompany] = useState(null);
  const [company,setCompany] = useState(location.state?.data);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isCreatingCompany, setIsCreatingCompany] = useState(false);
  const { nit } = useParams();
	const navigate = useNavigate(); 
  const [open,setOpen]=useState(false);

  useEffect(() => {
    let token = localStorage.getItem('token');
    if(!token){
      navigate('/login')
      return;
    };
    let parsedToken = parseJwt(token);
    if (parsedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'] === 'True') setIsAdmin(true);
    if (!nit) navigate('/companies'); // Navegar a la ruta /companies
    updateDataProducts();
  }, [nit]);

  const updateDataProducts=()=>{
    try {
      const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
      // Crear una instancia de Axios con la configuración de encabezado que incluye el token
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });

      axiosWithAuth.get(`company/${company.nit}/products`)
        .then((response)=>{
          setCompanies(response.data)
        });
    } catch (error) {
      console.error(error);
    }
  }

  const handleOpenModal = (company = null, isCreating = false) => {
    setSelectedCompany(company || { name: '', price: '' }); // Si no se proporciona una empresa, crear una nueva empresa vacía
    setIsCreatingCompany(isCreating);
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setSelectedCompany(null);
    setIsModalOpen(false);
  };

  const handleEdit = (product) => {
    setSelectedCompany(product);
    setIsCreatingCompany(false);
    setIsModalOpen(true);
  };

  const handleEditSubmit = async () => {
    try {
      const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
      
      // Crear una instancia de Axios con la configuración de encabezado que incluye el token
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });
      let dataProduct=selectedCompany;
      
      // Realizar la solicitud PUT con la instancia de Axios que incluye el token
      await axiosWithAuth.put(`company/${company.nit}/products/${selectedCompany.id}`, dataProduct);
      // const updatedCompanies = companies.map((c) => (c.id === selectedCompany.id ? selectedCompany : c));
      // setCompanies(updatedCompanies);
      updateDataProducts();
      handleCloseModal();
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async (product) => {
    try {
      if(window.confirm("Está seguro de eliminar este producto?")){
        const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
        // Crear una instancia de Axios con la configuración de encabezado que incluye el token
        const axiosWithAuth = axios.create({
          baseURL: process.env.REACT_APP_API_URL,
          headers: {
            Authorization: authToken,
            "Content-Type": "application/json"
          }
        });
        await axiosWithAuth.delete(`company/${company.nit}/products/${product.id}`);
        updateDataProducts();
      };
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteConfirm = async () => {
    try {
      if(window.confirm("Está seguro de eliminar este producto?")){
        const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
        // Crear una instancia de Axios con la configuración de encabezado que incluye el token
        const axiosWithAuth = axios.create({
          baseURL: process.env.REACT_APP_API_URL,
          headers: {
            Authorization: authToken,
            "Content-Type": "application/json"
          }
        });
        await axiosWithAuth.delete(`company/${company.nit}/products/${selectedCompany.id}`);
        updateDataProducts();
        handleCloseModal();
      }
    } catch (error) {
      console.error(error);
    }
  };

  function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  const handleCreateCompany = async () => {
    try {
      const authToken = "Bearer " + localStorage.getItem("token");
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });
      const response = await axiosWithAuth.post(`company/${company.nit}/products`, selectedCompany);
      const newCompany = response.data;
      const updatedCompanies = [...companies, newCompany];
      setCompanies(updatedCompanies);
      setIsModalOpen(false); // Cerrar el modal después de crear la empresa
    } catch (error) {
      console.error(error);
      alert(error.response.data);
    }
  };

  const handleRedirectProducts=()=>{
    navigate('/companies');
  }

  function formatNumber(num) {
    // let number = parseInt(num.replace(/\./g, ''));
    // console.log(number.toLocaleString());
    return num.toLocaleString();
  }

  const downloadExcelFile = () => {
    // Obtener los datos que deseas descargar
    const data = companies;
  
    // Convertir los datos a formato de hoja de cálculo
    const sheet = XLSX.utils.json_to_sheet(data);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, sheet, 'Datos');
  
    // Descargar el archivo
    const fileBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const file = new Blob([fileBuffer], { type: 'application/octet-stream' });
    saveAs(file, 'Productos.xlsx');
  }
  
  const handleClick = () => {
    console.log(!open);
  	setOpen(!open);
  }

  const handleLogout=()=>{
    localStorage.clear();
    navigate('/login');
  }

  return (
		<>
    <header>
      <img className="logo-header" src={Logo} alt="Logo de la aplicación" />
      <div className="logout-button">
        {/* <button onClick={handleLogout}>Cerrar sesión</button> */}
        <div className="link">
    	  <span onClick={()=>handleClick()}>Menú</span>
        <div className={`menuA ${open ? 'open' : ''}`}>
          <ul>
            <li>Perfil</li>
            <li onClick={handleLogout}>Cerrar sesión</li>
          </ul>
        </div>
    	</div>
      </div>
    </header>
    <div className="company-list-container">
      <div className="company-list-header">
        <h1>Lista de productos de <strong>{company?.name}</strong></h1>
        {isAdmin&&<button className="company-list-button" onClick={() => handleOpenModal(null, true)}>Agregar productos</button>}
        <button className="company-list-button" onClick={() => downloadExcelFile()}>Descargar reporte</button>
      </div>
      <div className="company-list-header">
        <button className="company-list-button" onClick={() => handleRedirectProducts()}>Volver</button>
      </div>
      <ul className="company-list">
        {companies.map((comp) => (
          <li key={comp.id} className="company-list-item">
            <div className="company-list-item-column">
              <h2>{comp.name}</h2>
            </div>
            <div className="company-list-item-column">
              <p>{'$'+comp.price.toLocaleString()}</p>
            </div>
            <div className="company-list-item-column">
              <button className="company-list-button edit" onClick={() => handleEdit(comp)}>{isAdmin?"Editar":"Ver"}</button>
              {isAdmin&&<button className="company-list-button delete" onClick={() => handleDelete(comp)}>Eliminar</button>}
            </div>
          </li>
        ))}
      </ul>
      {selectedCompany && (
        <div className="modal">
          <div className="modal-content apple">
            {isModalOpen && (
              <>
                <h2>{isCreatingCompany ? "Crear producto" : (isAdmin ? "Editar o eliminar" : "Ver") + `${selectedCompany.name}`}</h2>
                <p>Nombre del producto</p>
                <input type="text" value={selectedCompany.name} onChange={(e) => setSelectedCompany({ ...selectedCompany, name: e.target.value })} disabled={!isAdmin && !isCreatingCompany}/>
                <p>Valor</p>
                {/* <input type="number" step="any" value={selectedCompany.price.toLocaleString()} onChange={(e) => setSelectedCompany({ ...selectedCompany, price: e.target.value })} disabled={!isAdmin && !isCreatingCompany}/> */}
                <input 
                  type="number" 
                  step={100} 
                  value={selectedCompany.price} 
                  onChange={(e) => {
                    setSelectedCompany({ ...selectedCompany, price: e.target.value });
                  }} 
                  disabled={!isAdmin && !isCreatingCompany}
                />
                {isAdmin && !isCreatingCompany ? <button className="btn" onClick={handleEditSubmit}>Guardar cambios</button> : null}
                {isCreatingCompany ? <button className="btn" onClick={handleCreateCompany} disabled={!Object.values(selectedCompany).every(Boolean)}>Crear producto</button> : null}
                {isAdmin && !isCreatingCompany ? <button className="btn-delete" onClick={handleDeleteConfirm}>Eliminar</button> : null}
              </>
            )}
            <button className="btn" onClick={handleCloseModal}>Cerrar</button>
          </div>
        </div>
      )}
    </div>
		</>
  );
};

export default ProductList;

