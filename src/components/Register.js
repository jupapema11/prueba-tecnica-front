import React, { Component } from 'react';
import styled from 'styled-components';
import Logo from '../assets/img/logo.png';
import { Navigate } from 'react-router-dom';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100vh;
`;

const LogoImage = styled.img`
    width: 180px;
    height: 180px;
    margin-bottom: 2rem;
`;

const Form = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    max-width: 400px;
    margin: 0 auto;
    padding: 2rem;
    background-color: #f2f2f7;
    border-radius: 16px;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
`;

const Label = styled.label`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin-bottom: 1rem;
    font-size: 1rem;
    font-weight: 600;
    color: #8e8e93;
`;

const Input = styled.input`
    padding: 1rem;
    border none;
    border-radius: 8px;
    font-size: 1rem;
    margin-top: 0.5rem;
    background-color: #ffffff;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const Button = styled.button`
    padding: 1rem 2rem;
    background-color: #007aff;
    color: #ffffff;
    font-size: 1rem;
    font-weight: 600;
    border: none;
    border-radius: 8px;
    cursor: pointer;
    transition: background-color 0.2s;

    &:hover {
        background-color: #005ad6;
    }
`;

const Error = styled.p`
    color: #ff3b30;
    font-size: 0.8rem;
    margin-top: 0.5rem;
`;


class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            showConfirmPassword: false,
            errors: {},
            shouldRedirect: false 
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        const { email, password, confirmPassword } = this.state;

        // Validar campos
        const errors = {};

        if (!email) {
            errors.email = 'El correo electrónico es requerido';
        }

        if (!password) {
            errors.password = 'La contraseña es requerida';
        }

        if (!confirmPassword) {
            errors.confirmPassword = 'La confirmación de contraseña es requerida';
        }

        if (password !== confirmPassword) {
            errors.confirmPassword = 'Las contraseñas no coinciden';
        }

        if (Object.keys(errors).length > 0) {
            this.setState({ errors });
            return;
        }

        // Enviar datos del formulario al servidor
        fetch(process.env.REACT_APP_API_URL+'Account/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Origin': 'http://localhost:3000'
            },
            body: JSON.stringify({
                email,
                password,
                confirmPassword
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Error al registrar usuario');
            }

            console.log('Usuario registrado exitosamente');
            // localStorage.setItem()
            // history.push('/companies');
            this.login(event);
        })
        .catch(error => {
            console.error(error);
        });
    }

    
    login(event){
        event.preventDefault();
        const { email, password } = this.state;
        fetch(process.env.REACT_APP_API_URL+'Account/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Origin': 'http://localhost:3000'
            },
            body: JSON.stringify({
                email,
                password,
                "rememberMe":true
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Error al iniciar sesion');
            }

            console.log('Login response-->',response);
            return response.json();
            

        }).then(res=>{
            console.log(res)
            localStorage.setItem("token",res.token);
            this.setState({ shouldRedirect: true });
        })
        .catch(error => {
            console.error(error);
        });
    } 

    render() {
        const { errors } = this.state;
        if (this.state.shouldRedirect) {
            return <Navigate to='/companies' />;
        }
        return (
            <Container>
                <LogoImage src={Logo} alt="Logo de la aplicación" />
                <Form onSubmit={this.handleSubmit}>
                    <Label>
                        Correo electrónico:
                        <Input
                            name="email"
                            type="email"
                            value={this.state.email}
                            onChange={this.handleInputChange}
                        /> 
                        {errors.email && <Error>{errors.email}</Error>}
                    </Label>

                    <Label>
                        Contraseña:
                        <Input
                            name="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.handleInputChange}
                        />
                        {errors.password && <Error>{errors.password}</Error>}
                    </Label>

                    <Label>
                        Confirmar contraseña:
                        <Input
                            name="confirmPassword"
                            type="password"
                            value={this.state.confirmPassword}
                            onChange={this.handleInputChange}
                        />
                        {errors.confirmPassword && <Error>{errors.confirmPassword}</Error>}
                    </Label>

                    <Button type="submit">Registrarme</Button>
                </Form>
            </Container>
        );
    }
}

export default Register;
