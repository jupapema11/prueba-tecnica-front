
import './App.css';
import React from 'react';
import { Routes,Route } from 'react-router-dom';
import Login from './components/Login';
import Register from './components/Register';
import CompanyList from './components/CompanyList';
import ProductList from './components/ProductList';
import { Navigate } from 'react-router-dom';


function App() {
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/companies" element={<CompanyList />} />
      <Route path="/products/:nit" element={<ProductList />} />
      <Route path="*" element={<Navigate to="/companies" />} />
    </Routes>
  );
}

export default App;
