import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../assets/css/CompanyList.css';
import { useNavigate } from 'react-router-dom';
import Logo from '../assets/img/logo.png';
// import * as XLSX from 'xlsx';

const CompanyList = () => {
  const [companies, setCompanies] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedCompany, setSelectedCompany] = useState(null);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isCreatingCompany, setIsCreatingCompany] = useState(false);
  const [nit, setNit] = useState("");
	const navigate = useNavigate();
  const [open,setOpen]=useState(false);

  useEffect(() => {
    let token = localStorage.getItem("token");
    if(!token){
      navigate('/login')
      return;
    };
    const fetchCompanies = async () => {
      const response = await axios.get(process.env.REACT_APP_API_URL+'Company');
      setCompanies(response.data);
    };
    fetchCompanies();
    let parsedToken=parseJwt(token);
    if(parsedToken["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]==="True") setIsAdmin(true)
    console.log("token::::::",parsedToken);
  }, []);

  const handleOpenModal = (company = null, isCreating = false) => {
    setSelectedCompany(company || { name: '', address: '', phone: '' }); // Si no se proporciona una empresa, crear una nueva empresa vacía
    setIsCreatingCompany(isCreating);
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setSelectedCompany(null);
    setIsModalOpen(false);
  };

  const handleEdit = (company) => {
    setSelectedCompany(company);
    setIsCreatingCompany(false);
    setIsModalOpen(true);
  };

  const handleEditSubmit = async () => {
    try {
      const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
      
      // Crear una instancia de Axios con la configuración de encabezado que incluye el token
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });
      
      // Realizar la solicitud PUT con la instancia de Axios que incluye el token
      await axiosWithAuth.put(`Company/${selectedCompany.nit}`, selectedCompany);
      const updatedCompanies = companies.map((c) => (c.nit === selectedCompany.nit ? selectedCompany : c));
      setCompanies(updatedCompanies);
      handleCloseModal();
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async (company) => {
    try {
      if(!window.confirm("Está seguro de eliminar este producto?")){
        return;
      }
      const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
      
      // Crear una instancia de Axios con la configuración de encabezado que incluye el token
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });
      
      // Realizar la solicitud DELETE con la instancia de Axios que incluye el token
      await axiosWithAuth.delete(`Company/${company.nit}`);
      const updatedCompanies = companies.filter((c) => c.nit !== company.nit);
      setCompanies(updatedCompanies);
    } catch (error) {
      console.error(error);
      alert(error.response.data);
    }
  };

  const handleDeleteConfirm = async () => {
    try {
      if(!window.confirm("Está seguro de eliminar este producto?")){
        return;
      }
      const authToken = "Bearer " + localStorage.getItem("token"); // Obtener el token de autenticación
      
      // Crear una instancia de Axios con la configuración de encabezado que incluye el token
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });
      await axiosWithAuth.delete(process.env.REACT_APP_API_URL+`Company/${selectedCompany.nit}`);
      const updatedCompanies = companies.filter((c) => c.nit !== selectedCompany.nit);
      setCompanies(updatedCompanies);
      handleCloseModal();
    } catch (error) {
      console.error(error);
      alert(error.response.data);
    }
  };

  function parseJwt (token) {
    if(!token) return;
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  const handleCreateCompany = async () => {
    try {
      const authToken = "Bearer " + localStorage.getItem("token");
      const axiosWithAuth = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
          Authorization: authToken,
          "Content-Type": "application/json"
        }
      });
      const response = await axiosWithAuth.post(`Company`, selectedCompany);
      const newCompany = response.data;
      const updatedCompanies = [...companies, newCompany];
      setCompanies(updatedCompanies);
      setIsModalOpen(false); // Cerrar el modal después de crear la empresa
    } catch (error) {
      console.error(error);
      alert(error.response.data);
    }
  };

  const handleRedirectProducts=(elem)=>{
    navigate('/products/'+elem.nit,{state:{data:elem}});
		handleCloseModal();
  }

  const handleLogout=()=>{
    localStorage.clear();
    navigate('/login');
  }

  const handleClick = () => {
    console.log(!open);
  	setOpen(!open);
  }

  // const downloadExcelFile = () => {
  //   // Obtener los datos que deseas descargar
  //   const data = companies;
  
  //   // Convertir los datos a formato de hoja de cálculo
  //   const sheet = XLSX.utils.json_to_sheet(data);
  //   const workbook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(workbook, sheet, 'Datos');
  
  //   // Descargar el archivo
  //   const fileBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
  //   const file = new Blob([fileBuffer], { type: 'application/octet-stream' });
  //   saveAs(file, 'datos.xlsx');
  // }

  return (
		<>
		<header>
      <img className="logo-header" src={Logo} alt="Logo de la aplicación" />
      <div className="logout-button">
        {/* <button onClick={handleLogout}>Cerrar sesión</button> */}
        <div className="link">
    	  <span onClick={()=>handleClick()}>Menú</span>
        <div className={`menuA ${open ? 'open' : ''}`}>
          <ul>
            <li>Perfil</li>
            <li onClick={handleLogout}>Cerrar sesión</li>
          </ul>
        </div>
    	</div>
      </div>
    </header>
    <div className="company-list-container">
      <div className="company-list-header">
        <h1>Lista de empresas</h1>
        {isAdmin&&<button className="company-list-button" onClick={() => handleOpenModal(null, true)}>Agregar empresa</button>}
      </div>
      <ul className="company-list">
        {companies.map((company) => (
          <li key={company.nit} className="company-list-item">
            <div className="company-list-item-column">
              <h2>{company.name}</h2>
            </div>
            <div className="company-list-item-column">
              <p>{company.address}</p>
            </div>
            <div className="company-list-item-column">
              <p>{company.phone}</p>
            </div>
            <div className="company-list-item-column">
              <button className="company-list-button edit" onClick={() => handleEdit(company)}>{isAdmin?"Editar":"Ver"}</button>
              {isAdmin&&<button className="company-list-button delete" onClick={() => handleDelete(company)}>Eliminar</button>}
            </div>
          </li>
        ))}
      </ul>
      {selectedCompany && (
        <div className="modal">
          <div className="modal-content apple">
            {isModalOpen && (
              <>
                <h2>{isCreatingCompany ? "Crear empresa" : (isAdmin ? "Editar o eliminar" : "Ver") + `${selectedCompany.name}`}</h2>
                {isCreatingCompany ? (
                  <>
                  <p>Nit</p>
                  <input type="text" value={selectedCompany.nit} onChange={(e) => setSelectedCompany({ ...selectedCompany, nit: e.target.value })} disabled={!isAdmin && !isCreatingCompany}/>
                  </>
                ) : null}
                <p>Nombre</p>
                <input type="text" value={selectedCompany.name} onChange={(e) => setSelectedCompany({ ...selectedCompany, name: e.target.value })} disabled={!isAdmin && !isCreatingCompany}/>
                <p>Direccion</p>
                <input type="text" value={selectedCompany.address} onChange={(e) => setSelectedCompany({ ...selectedCompany, address: e.target.value })} disabled={!isAdmin && !isCreatingCompany}/>
                <p>Teléfono</p>
                <input type="text" value={selectedCompany.phone} onChange={(e) => setSelectedCompany({ ...selectedCompany, phone: e.target.value })} disabled={!isAdmin && !isCreatingCompany}/>
                {isAdmin && !isCreatingCompany ? <button className="btn button-78" onClick={handleEditSubmit}>Guardar cambios</button> : null}
                {isCreatingCompany ? <button className="btn button-78" onClick={handleCreateCompany} disabled={!Object.values(selectedCompany).every(Boolean)}>Crear empresa</button> : null}
                {!isCreatingCompany ? <button className="btn button-78" onClick={()=>handleRedirectProducts(selectedCompany)}>Ver productos</button> : null}
                {isAdmin && !isCreatingCompany ? <button className="btn-delete button-78" onClick={handleDeleteConfirm}>Eliminar</button> : null}
              </>
            )}
            <button className="btn button-78" onClick={handleCloseModal}>Cerrar</button>
          </div>
        </div>
      )}
    </div>
		</>
  );
};

export default CompanyList;
