import React, { Component } from 'react';
import styled from 'styled-components';
import { Navigate } from 'react-router-dom';

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
`;

const Form = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    max-width: 400px;
    margin: 0 auto;
    padding: 2rem;
    background-color: #f2f2f7;
    border-radius: 16px;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
`;

const Label = styled.label`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin-bottom: 1rem;
    font-size: 1rem;
    font-weight: 600;
    color: #8e8e93;
`;

const Input = styled.input`
    padding: 1rem;
    border: none;
    border-radius: 8px;
    font-size: 1rem;
    margin-top: 0.5rem;
    background-color: #ffffff;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const Button = styled.button`
    padding: 1rem 2rem;
    background-color: #007aff;
    color: #ffffff;
    font-size: 1rem;
    font-weight: 600;
    border: none;
    border-radius: 8px;
    cursor: pointer;
    transition: background-color 0.2s;

    &:hover {
        background-color: #005ad6;
    }
`;

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            showConfirmPassword: false,
            shouldRedirect: false 
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    
    handleSubmit(event) {
        event.preventDefault();

        const { email, password } = this.state;

        // Aquí es donde enviarías los datos del formulario a tu servidor para autenticar al usuario
        fetch(process.env.REACT_APP_API_URL+'Account/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password,
                rememberMe:true
            })
        })
        .then(response => response.json())
        .then(res => {
            console.log(res);
            localStorage.setItem("token",res.token);
            this.setState({ shouldRedirect: true });
            // Aquí es donde manejarías la respuesta del servidor
        })
        .catch(error => {
            console.error(error);
            // Aquí es donde manejarías los errores
        });
    }

    render() {
        if (this.state.shouldRedirect) {
            return <Navigate to='/companies' />;
        }
        return (
            <Container>
                <Form onSubmit={this.handleSubmit}>
                    <Label>
                        Correo electrónico:
                        <Input
                            name="email"
                            type="email"
                            value={this.state.email}
                            onChange={this.handleInputChange}
                        /> 
                    </Label>

                    <Label>
                        Contraseña:
                        <Input
                            name="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.handleInputChange}
                        />
                    </Label>

                    <Button type="submit">Iniciar sesión</Button>
                </Form>
            </Container>
        );
    }
}

export default Login;
